window.onload = function() {
    class User {
        constructor(apiKey, name) {
            this.apiURL = `https://proxy6.net/api/${apiKey}`;
            this.getContentFromUrl = `https://proxy.piar4u.com/getContentFromUrl.php`;
            this.name = name;
            this.balance = 0;
            this.proxyList = {};
            this.currency = `\u{20BD}`;
        }
        async sentRequest(method, requestBody) {
            let response = await fetch(`${this.getContentFromUrl}?ext_url=${this.apiURL}/${method}?${requestBody}`);
            let json = JSON.parse(await response.json());
            decorateProxyValues(json);
            this.balance = json.balance;
            return json;
        }
        async getProxy(state = "all", descr = "") {
            let requestBody = `state=${state}%26descr=${descr}`;
            let json = await this.sentRequest("getproxy", requestBody);
            this.proxyList = json.list;
            return json;
        }
        async getPrice(count, period, version = "6") {
            let requestBody = `count=${count}%26period=${period}%26version=${version}`;
            let json = await this.sentRequest("getprice", requestBody);
            return json;
        }
        async getCount(country, version = "6") {
            let requestBody = `country=${country}%26version=${version}`;
            let json = await this.sentRequest("getcount", requestBody);
            return json;
        }
        async getCountry(version = "6") {
            let requestBody = `version=${version}`;
            let json = await this.sentRequest("getcountry", requestBody);
            return json;
        }
        async setType(ids, type) {
            let requestBody = `ids=${ids}%26type=${type}`;
            let json = await this.sentRequest("settype", requestBody);
            return json;
        }
        async buy(count, period, country, version = "6", type = "http") {
            let requestBody = `count=${count}%26period=${period}%26country=${country}%26version=${version}%26type=${type}`;
            let json = await this.sentRequest("buy", requestBody);
            return json;
        }
        async prolong(period, ids) {
            let requestBody = `period=${period}%26ids=${ids}`;
            let json = await this.sentRequest("prolong", requestBody);
            for (let item in json.list) {
                this.proxyList[item].TTL = json.list[item].TTL;
                this.proxyList[item].date_end = json.list[item].date_end;
            }
            return json;
        }
        async setDescr(newComment, ids) {
            let requestBody = `new=${newComment}%26ids=${ids}`;
            let json = await this.sentRequest("setdescr", requestBody);
            return json;
        }
        async delete(ids) {
            let requestBody = `ids=${ids}`;
            let json = await this.sentRequest("delete", requestBody);
            return json;
        }
        async check(ids) {
            let requestBody = `ids=${ids}`;
            let json = await this.sentRequest("check", requestBody);
            return json;
        }
    }

    let AVO = new User("66bf711336-984bf1c504-b1fb81e03e", "AVO");

    initialization(AVO);

    function initialization(user) {
        user.getProxy().then(() => {
            displayBalanceAndUsername(user);
            enableButtons();
            let orderForms = document.querySelectorAll(".proxy_type_container");
            addEventListeners(orderForms, "change", checkOrder);
            addEventListeners(orderForms, "click", makeOrder);
        });

        function checkOrder() {
            let currentForm = event.target.form;
            let orderProxyType = currentForm.closest(".proxy_type_container");
            let priceContainer = orderProxyType.querySelector(".total_price");
            let updatePriceAnimation = priceContainer.querySelector(".update_price");
            updatePriceAnimation.classList.remove("hide");
            let selectedVersion = orderProxyType.getAttribute("data-type");
            let selectedCounty = currentForm.elements.selectedCounty.value;
            let enteredCount = currentForm.elements.enteredCount.value;
            let selectedPeriod = currentForm.elements.selectedPeriod.value;
            let selectedProtocol = currentForm.elements.selectedProtocol.value;
            orderProxyType.setAttribute("data-country", selectedCounty);
            orderProxyType.setAttribute("data-count", enteredCount);
            orderProxyType.setAttribute("data-period", selectedPeriod);
            orderProxyType.setAttribute("data-protocol", selectedProtocol);

            let json = user.getPrice(enteredCount, selectedPeriod, selectedVersion);
            json.then((response) => {
                updatePriceAnimation.classList.add("hide");
                priceContainer.firstElementChild.innerText = response.price.toFixed(2);
            })
        }

        function makeOrder() {
            event.preventDefault();
            if (event.target.type === "submit") {
                let orderProxyType = event.target.closest(".proxy_type_container");
                let popup = orderInProgressWindow();
                orderProxyType.prepend(popup);

                let selectedVersion = orderProxyType.getAttribute("data-type");
                let selectedCounty = orderProxyType.getAttribute("data-country");
                let enteredCount = orderProxyType.getAttribute("data-count");
                let selectedPeriod = orderProxyType.getAttribute("data-period");
                let selectedProtocol = orderProxyType.getAttribute("data-protocol");

                let json = user.buy(enteredCount, selectedPeriod, selectedCounty, selectedVersion, selectedProtocol);
                json.then((response) => {
                    if (response.status === "yes") {
                        let userBalanceValue = document.querySelector(".user_balance b");
                        userBalanceValue.innerText = `${user.balance} ${user.currency}`;
                        popup.innerHTML = `<p>${response.count} proxies were successfully added to your proxy list!</p>`;
                        setTimeout(() => {
                            popup.parentNode.removeChild(popup);
                        }, 3500);
                    } else {
                        if (response.error_id === 300) {
                            popup.innerHTML = `<p style="color: red;">This number of proxies is not available</p>`;
                        } else if (response.error_id === 400) {
                            popup.innerHTML = `<p style="color: red;">Unfortunately, you have a low balance</p>`;
                        } else {
                            popup.innerHTML = `<p style="color: red;">Something went wrong :(</p>`;
                        }
                        setTimeout(() => {
                            popup.parentNode.removeChild(popup);
                        }, 4000);
                    }
                })
            }
        }
    }

    function displayBalanceAndUsername(user) {
        let userBalance = document.querySelector(".user_balance");
        userBalance.innerHTML = `<b>${user.balance} ${user.currency}</b>`;
        let userName = document.querySelector(".username");
        userName.innerHTML = `${user.name}`;
    }

    function addEventListeners(elements, eventType, handler) {
        for (let i = 0; i < elements.length; i++) {
            elements[i].addEventListener(eventType, handler)
        }
    }

    function orderInProgressWindow() {
        let popup = document.createElement('div');
        popup.classList.add('order_in_progress', 'animated', 'zoomIn');
        let template = `<div>
                            Order in progress <i class="fas fa-sync-alt animate_proxy_operations_inner"</i>
                        </div>`;
        popup.innerHTML = template;
        return popup;
    }

    function getTimeToExpiration(stringDate) {
        let dateNow = new Date();
        let expirationDate = Date.parse(stringDate);
        let difference = expirationDate - dateNow;
        let msMinute = 1000 * 60;
        let msInHour = 1000 * 60 * 60;
        let fullHours = Math.floor(difference / msInHour);
        let fullMinutes = Math.floor(difference / msMinute);
        let days = Math.floor(fullHours / 24);
        let hours = (days === 0 ? Math.floor(fullHours) : Math.floor(fullHours - 24 * days))
        return [`${days}d ${hours}h`, fullHours, fullMinutes];
    }

    function enableButtons() {
        let submitButtons = document.querySelectorAll(`input[type="submit"]`);
        for (button of submitButtons) {
            button.removeAttribute("disabled");
        }
    }

    function transformDate(stringDate) {
        let date = new Date(Date.parse(stringDate));
        let month = parseInt(date.getMonth()) + 1;
        let day = parseInt(date.getDate());
        let hour = parseInt(date.getHours());
        let minute = parseInt(date.getMinutes());
        if (month < 10) {
            month = `0` + month;
        }
        if (day < 10) {
            day = `0` + day;
        }
        if (hour < 10) {
            hour = `0` + hour;
        }
        if (minute < 10) {
            minute = `0` + minute;
        }
        return `${day}-${month}-${date.getFullYear()} ${hour}:${minute}`
    }

    function decorateProxyValues(json) {
        for (let item in json.list) {
            let TTL = getTimeToExpiration(json.list[item].date_end);
            json.list[item]["TTL"] = TTL[0];
            json.list[item]["TTL_hours"] = TTL[1];
            json.list[item]["TTL_minutes"] = TTL[2];
            json.list[item].date_end = transformDate(json.list[item].date_end);
            if (json.list[item].type === "socks") {
                json.list[item].type = "SOCKS5";
            } else if (json.list[item].type === "http") {
                json.list[item].type = "HTTPs";
            }
            if (json.list[item].version === "6") {
                json.list[item].version = "IPv6";
            } else if (json.list[item].version === "4") {
                json.list[item].version = "IPv4";
            } else if (json.list[item].version === "3") {
                json.list[item].version = "IPv4s";
            }

        }
    }
}