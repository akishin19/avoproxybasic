window.onload = function() {
    class User {
        constructor(apiKey, name) {
            this.apiURL = `https://proxy6.net/api/${apiKey}`;
            this.getContentFromUrl = `https://proxy.piar4u.com/getContentFromUrl.php`;
            this.name = name;
            this.balance = 0;
            this.proxyList = {};
            this.currency = `\u{20BD}`;
        }
        async sentRequest(method, requestBody) {
            let response = await fetch(`${this.getContentFromUrl}?ext_url=${this.apiURL}/${method}?${requestBody}`);
            let json = JSON.parse(await response.json());
            decorateProxyValues(json);
            this.balance = json.balance;
            return json;
        }
        async getProxy(state = "all", descr = "") {
            let requestBody = `state=${state}%26descr=${descr}`;
            let json = await this.sentRequest("getproxy", requestBody);
            this.proxyList = json.list;
            return json;
        }
        async getPrice(count, period, version = "6") {
            let requestBody = `count=${count}%26period=${period}%26version=${version}`;
            let json = await this.sentRequest("getprice", requestBody);
            return json;
        }
        async getCount(country, version = "6") {
            let requestBody = `country=${country}%26version=${version}`;
            let json = await this.sentRequest("getcount", requestBody);
            return json;
        }
        async getCountry(version = "6") {
            let requestBody = `version=${version}`;
            let json = await this.sentRequest("getcountry", requestBody);
            return json;
        }
        async setType(ids, type) {
            let requestBody = `ids=${ids}%26type=${type}`;
            let json = await this.sentRequest("settype", requestBody);
            return json;
        }
        async buy(count, period, country, version = "6", type = "http") {
            let requestBody = `count=${count}%26period=${period}%26country=${country}%26version=${version}%26type=${type}`;
            let json = await this.sentRequest("buy", requestBody);
            return json;
        }
        async prolong(period, ids) {
            let requestBody = `period=${period}%26ids=${ids}`;
            let json = await this.sentRequest("prolong", requestBody);
            for (let item in json.list) {
                this.proxyList[item].TTL = json.list[item].TTL;
                this.proxyList[item].date_end = json.list[item].date_end;
            }
            return json;
        }
        async setDescr(newComment, ids) {
            let requestBody = `new=${newComment}%26ids=${ids}`;
            let json = await this.sentRequest("setdescr", requestBody);
            return json;
        }
        async delete(ids) {
            let requestBody = `ids=${ids}`;
            let json = await this.sentRequest("delete", requestBody);
            return json;
        }
        async check(ids) {
            let requestBody = `ids=${ids}`;
            let json = await this.sentRequest("check", requestBody);
            return json;
        }
    }

    let AVO = new User("66bf711336-984bf1c504-b1fb81e03e", "AVO");

    initialization(AVO);

    function initialization(user) {
        user.getProxy().then(() => {
            displayBalanceAndUsername(user);
            if (document.title === "AVOPROXY") return;
            createProxyList(user);
            if (Object.keys(user.proxyList).length === 0) {
                emptyProxyWindow();
                return;
            }
            let checkButtons = document.querySelectorAll(".proxy_operations_outter[data-action=check]");
            let deleteButtons = document.querySelectorAll(".proxy_operations_outter[data-action=delete]");
            let addCommentButtons = document.querySelectorAll("a[data-action=add-comment]");
            addEventListeners(checkButtons, "click", checkProxy.bind(user));
            addEventListeners(deleteButtons, "click", deleteProxy.bind(user));
            addEventListeners(addCommentButtons, "click", showModalWin);
            let proxyListContainer = document.querySelector(`.proxy_list_container`);
            proxyListContainer.addEventListener("change", selectAllItems);
            let operationButtons = document.querySelector(`.operations`);
            operationButtons.addEventListener("click", initOperation);
            let settingsButton = document.querySelector(`.change_settings`);
            settingsButton.addEventListener("click", changeSettings);

            function changeSettings() {
                if (event.target.tagName !== "SELECT") return;
                let items = document.querySelectorAll(".proxy_item");
                let formVersion = document.forms["Version"];
                let formType = document.forms["Type"];
                let formSort = document.forms["Sort"];
                let selectedVersion = formVersion.elements.selectedVersion.value;
                let selectedType = formType.elements.selectedIPType.value;
                let selectedSort = formSort.elements.selectedSortType.value;
                filterItems(selectedVersion, selectedType, items);
                getSortedItems(items, selectedSort);
            }

            function initOperation() {
                if (this.getAttribute("data-status") === "inactive") return;
                let selectedItemsCheckboxes = Array.from(document.querySelectorAll(`.proxy_list input[data-status="active"]`));
                let selectedItemsIDs = selectedItemsCheckboxes.map(item => item.closest(".proxy_item").getAttribute("data-proxy_id"));
                if (event.target.hasAttribute("data-params") ||
                    event.target.parentElement.hasAttribute("data-params")) {
                    let notice = createNotice("Action in progress, please wait...");
                    let selectedAction = event.target.closest("li");
                    let json = user.setType(selectedItemsIDs, selectedAction.getAttribute("data-params"));
                    json.then((response) => {
                        let notice = document.querySelector(".notice_contaier");
                        if (response.status === "yes") {
                            notice.parentNode.removeChild(notice);
                            createNotice("The proxies type has successfully changed", true, 4000);
                            let selectedItems = selectedItemsCheckboxes.map(item => item.closest(".proxy_item"));
                            if (selectedAction.getAttribute("data-params") === "http") {
                                for (let i = 0; i < selectedItems.length; i++) {
                                    let item = selectedItems[i].querySelector("div[data-proxy_ip_type]");
                                    item.setAttribute("data-proxy_ip_type", "HTTPs");
                                    item.innerText = "HTTPs";
                                    user.proxyList[item.closest(".proxy_item").getAttribute("data-proxy_id")].type = "HTTPs";
                                }
                            } else if ((selectedAction.getAttribute("data-params") === "socks")) {
                                for (let i = 0; i < selectedItems.length; i++) {
                                    let item = selectedItems[i].querySelector("div[data-proxy_ip_type]");
                                    item.setAttribute("data-proxy_ip_type", "SOCKS5");
                                    item.innerText = "SOCKS5";
                                    user.proxyList[item.closest(".proxy_item").getAttribute("data-proxy_id")].type = "SOCKS5";
                                }
                            }

                        } else {
                            notice.parentNode.removeChild(notice);
                            createNotice("These proxies already have the selected type", true, 4000);
                        }
                    })
                } else if (event.target.hasAttribute("prolong-period")) {
                    let notice = createNotice("Action in progress, please wait...");
                    let selectedPeriod = event.target.closest("li");
                    let json = user.prolong(selectedPeriod.getAttribute("prolong-period"), selectedItemsIDs);
                    json.then((response) => {
                        let notice = document.querySelector(".notice_contaier");
                        notice.parentElement.removeChild(notice);
                        if (response.status === "yes") {
                            createNotice(`The selected proxies was successfully renewed`, true, 3000);
                            let selectedItems = selectedItemsCheckboxes.map(item => item.closest(".proxy_item"));
                            for (let i = 0; i < selectedItems.length; i++) {
                                let itemDateEnd = selectedItems[i].querySelector(".date_value");
                                let itemTTL = selectedItems[i].querySelector(".days_value");
                                let userBalanceValue = document.querySelector(".user_balance b");
                                userBalanceValue.innerText = `${user.balance} ${user.currency}`;
                                itemDateEnd.innerText = user.proxyList[itemDateEnd.closest(".proxy_item").getAttribute("data-proxy_id")].date_end;
                                itemTTL.innerText = user.proxyList[itemDateEnd.closest(".proxy_item").getAttribute("data-proxy_id")].TTL;
                            }
                        } else {
                            createNotice(`Your balance is not enough funds. Proxy renewal costs ${response.price} ₽.
                            To pay the invoice you need an additional ` +
                                `${Math.floor((response.price - user.balance) * 100) / 100} ₽`, true, 8000);
                        }
                    })
                } else if (event.target.getAttribute("data-action") === "delete-selected") {
                    let notice = createNotice("Deleting items in progress, please wait...");
                    let json = user.delete(selectedItemsIDs);
                    json.then((response) => {
                        if (response.status === "yes") {
                            let notice = document.querySelector(".notice_contaier");
                            notice.parentElement.removeChild(notice);
                            for (let i = 0; i < response.count; i++) {
                                delete user.proxyList[selectedItemsIDs[i]];
                                let proxyItem = document.querySelector(`li[data-proxy_id="${selectedItemsIDs[i]}"]`);
                                proxyItem.parentElement.removeChild(proxyItem);
                            }
                        } else {
                            createNotice("Something went wrong. Please try again", true, 4000);
                        }
                        alert(`Count of proxies deleted: ${response.count}`);
                    });
                }
            }

            function selectAllItems() {
                let operations = document.querySelector(".operations");
                if (event.target.getAttribute("data-action") === "select-all") {
                    if (event.target.getAttribute("data-status") === "inactive") {
                        event.target.setAttribute("data-status", "active");
                        let listCheckboxes = document.querySelectorAll(`.proxy_list li[data-proxy_displayed="yes"] input[data-status="inactive"]`);
                        for (let i = 0; i < listCheckboxes.length; i++) {
                            listCheckboxes[i].checked = true;
                            listCheckboxes[i].setAttribute("data-status", "active");
                        }
                    } else {
                        event.target.setAttribute("data-status", "inactive");
                        let listCheckboxes = document.querySelectorAll(`.proxy_list input[data-status="active"]`);
                        for (let i = 0; i < listCheckboxes.length; i++) {
                            listCheckboxes[i].checked = false;
                            listCheckboxes[i].setAttribute("data-status", "inactive");
                        }
                    }
                } else if (event.target.getAttribute("data-action") === "select-item") {
                    if (event.target.getAttribute("data-status") === "inactive") {
                        event.target.setAttribute("data-status", "active");
                        event.target.checked = true;
                    } else {
                        event.target.setAttribute("data-status", "inactive");
                        event.target.checked = false;
                    }
                }
                let selectedItemsCount = document.querySelectorAll(`.proxy_list input[data-status="active"]`);
                let operationButtons = document.querySelectorAll(".items_count");
                for (let i = 0; i < operationButtons.length; i++) {
                    operationButtons[i].innerHTML = selectedItemsCount.length;
                }
                if (selectedItemsCount.length === 0) {
                    let selectAllButton = document.querySelector(`.outter input[data-action="select-all"]`);
                    selectAllButton.checked = false;
                    operations.setAttribute("data-status", "inactive");
                    operations.classList.add("operations_inactive");
                } else {
                    operations.setAttribute("data-status", "active");
                    operations.classList.remove("operations_inactive");
                }
            }

            function showModalWin() {
                event.preventDefault();
                let modal = createModalWindows();
                this.parentElement.append(modal);
                let popupInput = document.querySelector(".popup_input");
                let modalWin = document.querySelector('.popupWin');
                modalWin.style.display = 'flex';
                popupInput.focus();
                let submitButton = document.querySelector(".popup_button");
                let modalInput = document.querySelector(".popup_input");
                let darkLayer = document.querySelector(".shadow");
                darkLayer.onclick = function() {
                    darkLayer.parentNode.removeChild(darkLayer);
                    modal.remove();
                    return false;
                };
                submitButton.addEventListener("click", changeComment.bind(this));
                modalInput.onkeypress = function() {
                    if (event.keyCode == 13) {
                        event.preventDefault();
                        return false;
                    }
                }

                function changeComment() {
                    event.preventDefault();
                    this.nextElementSibling.innerHTML = `<img src="./img/commentUpdate.svg"></img>`;
                    let comment = document.querySelector(".popup_input").value;
                    submitButton.removeEventListener("click", changeComment.bind(this));
                    let darkLayer = document.querySelector(".shadow");
                    darkLayer.parentNode.removeChild(darkLayer);
                    modal.remove();
                    let proxyID = this.closest(".proxy_item").getAttribute("data-proxy_id");
                    let json = user.setDescr(comment, proxyID);
                    json.then((response) => {
                        if (response.status === "yes" && response.count > 0) {
                            user.proxyList[proxyID]["descr"] = comment;
                            this.nextElementSibling.innerHTML = comment;
                            this.closest(".proxy_item").setAttribute("data-proxy_comment", comment);
                        }
                    })
                }
            }

            function checkProxy() {
                if (event.target.closest(".proxy_operations_outter")) {
                    event.preventDefault();
                    let actonButton = event.target.closest(".proxy_operations_outter");
                    let proxyID = actonButton.closest(".proxy_item").getAttribute("data-proxy_id");
                    if ("proxyStatus" in user.proxyList[proxyID]) return;
                    actonButton.firstElementChild.classList.remove("fa-eye");
                    actonButton.firstElementChild.classList.add("fa-sync-alt", "animate_proxy_operations_inner");
                    actonButton.classList.add("cheking");
                    let json = user.check(proxyID);
                    json.then((response) => {
                        actonButton.classList.remove("cheking");
                        if (response.proxy_status === true) {
                            actonButton.firstElementChild.classList.remove("fa-sync-alt", "animate_proxy_operations_inner");
                            actonButton.firstElementChild.classList.add("fa-thumbs-up");
                            actonButton.classList.add("cheking_result_true");
                        } else {
                            actonButton.firstElementChild.classList.remove("fa-sync-alt", "animate_proxy_operations_inner");
                            actonButton.firstElementChild.classList.add("fa-thumbs-down");
                            actonButton.classList.add("cheking_result_false");
                        }
                        user.proxyList[proxyID]["proxyStatus"] = response.proxy_status;
                    })
                }
            }

            function deleteProxy() {
                if (event.target.closest(".proxy_operations_outter")) {
                    event.preventDefault();
                    let actonButton = event.target.closest(".proxy_operations_outter");
                    let proxyID = actonButton.closest(".proxy_item").getAttribute("data-proxy_id");
                    let proxyContainer = event.target.closest(".proxy_item");
                    let json = user.delete(proxyID);
                    json.then((response) => {
                        if (response.count > 0) {
                            delete user.proxyList[proxyID];
                            proxyContainer.remove();
                            alert(`Count of proxies deleted: ${response.count}`);
                        }
                    });
                }
            }

            function displayBalanceAndUsername(user) {
                let userBalance = document.querySelector(".user_balance");
                userBalance.innerHTML = `<b>${user.balance} ${user.currency}</b>`;
                let userName = document.querySelector(".username");
                userName.innerHTML = `${user.name}`;
            }

            function createProxyList(user) {
                let userProxyList = document.querySelector(".proxy_list");
                for (let item in user.proxyList) {
                    userProxyList.append(createProxyItemTemplate(user.proxyList[item]));
                }
            }
        });
    }

    function addEventListeners(elements, eventType, handler) {
        for (let i = 0; i < elements.length; i++) {
            elements[i].addEventListener(eventType, handler)
        }
    }

    function createModalWindows() {
        let darkLayer = document.createElement('div');
        darkLayer.classList.add('shadow');
        document.body.appendChild(darkLayer);
        let template = ` <h3> Type new comment </h3>
                            <hr>
                            <form>
                                <input class="popup_input" placeholder="Comment">
                                <input class="popup_button" type="button" value="OK">
                            </form>
                            <hr>`
        let window = document.createElement("div");
        window.classList.add("modalwin", "popupWin");
        window.innerHTML = template;
        return window;
    }

    function createNotice(message, autoremove = false, timeout = 4000) {
        let proxyListContainer = document.querySelector(".operations_container");
        let notice = document.createElement('div');
        notice.classList.add("notice_contaier");
        notice.classList.add("animated", "zoomIn");
        notice.innerText = message;
        proxyListContainer.prepend(notice);
        if (autoremove === true) {
            setTimeout(() => {
                notice.parentNode.removeChild(notice);
            }, timeout);
        }
    }

    function getColor(TTL) {
        if (TTL <= 6) {
            return ["red", "red_text"];
        } else if (TTL <= 72) {
            return ["orange", "orange_text"];
        } else return ["green", "green_text"];
    }

    function getTimeToExpiration(stringDate) {
        let dateNow = new Date();
        let expirationDate = Date.parse(stringDate);
        let difference = expirationDate - dateNow;
        let msMinute = 1000 * 60;
        let msInHour = 1000 * 60 * 60;
        let fullHours = Math.floor(difference / msInHour);
        let fullMinutes = Math.floor(difference / msMinute);
        let days = Math.floor(fullHours / 24);
        let hours = (days === 0 ? Math.floor(fullHours) : Math.floor(fullHours - 24 * days));
        if (fullHours < 0) return [`0d 0h`, fullHours, fullMinutes];
        return [`${days}d ${hours}h`, fullHours, fullMinutes];
    }

    function transformDate(stringDate) {
        let date = new Date(Date.parse(stringDate));
        let month = parseInt(date.getMonth()) + 1;
        let day = parseInt(date.getDate());
        let hour = parseInt(date.getHours());
        let minute = parseInt(date.getMinutes());
        if (month < 10) {
            month = `0` + month;
        }
        if (day < 10) {
            day = `0` + day;
        }
        if (hour < 10) {
            hour = `0` + hour;
        }
        if (minute < 10) {
            minute = `0` + minute;
        }
        return `${day}-${month}-${date.getFullYear()} ${hour}:${minute}`
    }

    function decorateProxyValues(json) {
        for (let item in json.list) {
            let TTL = getTimeToExpiration(json.list[item].date_end);
            json.list[item]["TTL"] = TTL[0];
            json.list[item]["TTL_hours"] = TTL[1];
            json.list[item]["TTL_minutes"] = TTL[2];
            json.list[item].date_end = transformDate(json.list[item].date_end);
            if (json.list[item].type === "socks") {
                json.list[item].type = "SOCKS5";
            } else if (json.list[item].type === "http") {
                json.list[item].type = "HTTPs";
            }
            if (json.list[item].version === "6") {
                json.list[item].version = "IPv6";
            } else if (json.list[item].version === "4") {
                json.list[item].version = "IPv4";
            } else if (json.list[item].version === "3") {
                json.list[item].version = "IPv4s";
            }

        }
    }

    function filterItems(selectedVersion, selectedType, items) {
        if (selectedVersion === "All" && selectedType === "All") {
            for (item of items) {
                item.classList.remove("hide");
                item.setAttribute("data-proxy_displayed", "yes");
            }
            return;
        } else if (selectedVersion === "All" && selectedType !== "All") {
            for (item of items) {
                let checkBox = item.querySelector(`input[data-status]`);
                turnOffCheckboxes(checkBox);
                item.classList.add("hide");
                item.setAttribute("data-proxy_displayed", "no");
            }
            for (item of items) {
                if (item.getAttribute("data-proxy_type") === selectedType) {
                    item.classList.remove("hide");
                    item.setAttribute("data-proxy_displayed", "yes");
                    turnOffCheckboxes(item);
                }
            }
            return;
        } else if (selectedVersion !== "All" && selectedType === "All") {
            for (item of items) {
                let checkBox = item.querySelector(`input[data-status]`);
                turnOffCheckboxes(checkBox);
                item.classList.add("hide");
                item.setAttribute("data-proxy_displayed", "no");
            }
            for (item of items) {
                if (item.getAttribute("data-proxy_ip_version") === selectedVersion) {
                    item.classList.remove("hide");
                    item.setAttribute("data-proxy_displayed", "yes");
                }
            }
            return;
        } else if (selectedVersion !== "All" && selectedType !== "All") {
            for (item of items) {
                let checkBox = item.querySelector(`input[data-status]`);
                turnOffCheckboxes(checkBox);
                item.classList.add("hide");
                item.setAttribute("data-proxy_displayed", "no");
            }
            for (item of items) {
                if (item.getAttribute("data-proxy_type") === selectedType &&
                    item.getAttribute("data-proxy_ip_version") === selectedVersion
                ) {
                    item.classList.remove("hide");
                    item.setAttribute("data-proxy_displayed", "yes");
                }
            }
            return;
        }
    }

    function getSortedItems(items, sortType) {
        let parrent = items[0].parentNode;
        sortedItems = Array.from(items);
        if (sortType === "sort-exp_date_up") {
            sortedItems.sort((a, b) => {
                return parseInt(a.getAttribute("data-proxy_ttl_minutes")) -
                    parseInt(b.getAttribute("data-proxy_ttl_minutes"))
            });
        } else if (sortType === "sort-exp_date_down") {
            sortedItems.sort((a, b) => {
                return parseInt(b.getAttribute("data-proxy_ttl_minutes")) -
                    parseInt(a.getAttribute("data-proxy_ttl_minutes"))
            });
        } else if (sortType === "sort-comment-up") {
            sortedItems.sort((a, b) => { return a.getAttribute("data-proxy_comment").localeCompare(b.getAttribute("data-proxy_comment")) });
        } else if (sortType === "sort-comment-down") {
            sortedItems.sort((a, b) => { return b.getAttribute("data-proxy_comment").localeCompare(a.getAttribute("data-proxy_comment")) });
        }
        for (let i = 0; i < items.length; i++) {
            items[i].parentNode.removeChild(items[i]);
        }
        for (let i = 0; i < sortedItems.length; i++) {
            parrent.appendChild(sortedItems[i]);
        }
    }

    function turnOffCheckboxes(checkBox) {
        let selectAllButton = document.querySelector(`.outter input[data-action="select-all"]`);
        selectAllButton.checked = false;
        selectAllButton.setAttribute("data-status", "inactive");
        if (checkBox.getAttribute("data-status") === "active") {
            checkBox.checked = false;
            checkBox.setAttribute("data-status", "inactive");
        }
    }

    function emptyProxyWindow() {
        let message = `Unfortunately, you don’t have a proxy yet, but it’s easy to<a href="./buy-proxy.html"> change!</a>`;
        let parrent = document.querySelector(".proxy_list");
        let element = document.createElement("div");
        element.classList.add("empty_proxy_list");
        element.innerHTML = message;
        parrent.appendChild(element);
    }

    function createProxyItemTemplate(item) {
        let template = `<div class="proxy_item_container animated zoomIn">
        <div class="chekbox_contaier">
        <div class="checkbox_container">
            <input type="checkbox" class="checkbox" id="checkbox" data-status="inactive" data-action="select-item"></input>
            <label for="checkbox"></label>
        </div>
    </div>
    <div class="ip_type">
        <p class="proxy_country">${item.country}</p>
        <p class="proxy_ip_type${item.version}">${item.version}</p>
    </div>
    <div class="proxy_info">
        <div class="proxy">
            <div class="proxy_address">Proxy <span>IP:PORT</span></div>
            <div class="dotted_line"></div>
            <div class="test proxy_address_value"><b>${item.host}:${item.port}</b></div>
        </div>
        <div class="proxy">
            <div class="proxy_login">Login</div>
            <div class="dotted_line"></div>
            <div class="proxy_login_value"><b>${item.user}</b></div>
        </div>
        <div class="proxy">
            <div class="proxy_password">Password</div>
            <div class="dotted_line"></div>
            <div class="proxy_password_value"><b>${item.pass}</b></div>
        </div>
        <div class="proxy">
            <div class="proxy_type">Type</div>
            <div class="dotted_line"></div>
            <div data-proxy_ip_type="${item.type}">${item.type}</div>
        </div>
        <div class="proxy">
            <div class="proxy_ipv6">${item.version}</div>
            <div class="dotted_line"></div>
            <div class="proxy_ipv6_value"><span>${item.ip}</span></div>
        </div>
    </div>
    <div class="proxy_due_date">
        <div class="due_date">
            <div class="date">Date</div>
            <div class="dotted_line"></div>
            <div class="date_value ${getColor(item.TTL_hours)[1]}">${item.date_end}</div>
        </div>
        <div class="due_days">
            <div class="days">Days</div>
            <div class="dotted_line"></div>
            <div class="days_value TTL_container ${getColor(item.TTL_hours)[0]}">${item.TTL}</div>
        </div>
    </div>
    <div class="proxy_comment">
        <a href="" class="add_comment" data-action="add-comment"><span>+</span> Comment</a>
        <div class="comment">${item.descr}</div>
    </div>
    <div class="proxy_operations">
        <a href="" class="proxy_operations_outter" data-action="check"><i class="fas fa-eye"></i></a>
        <a href="" class="proxy_operations_outter" data-action="delete"><i class="fas fa-trash-alt"></i></a>
        </a>
    </div>
    </div>`;
        let element = document.createElement("li");
        element.classList.add("proxy_item");
        element.setAttribute(`data-proxy_id`, item.id);
        element.setAttribute(`data-proxy_type`, item.type);
        element.setAttribute(`data-proxy_ttl`, item.TTL_hours);
        element.setAttribute(`data-proxy_ttl_minutes`, item.TTL_minutes);
        element.setAttribute(`data-proxy_comment`, item.descr);
        element.setAttribute(`data-proxy_ip_version`, item.version);
        element.setAttribute(`data-proxy_displayed`, "yes");
        element.innerHTML = template;
        return element;
    }
}