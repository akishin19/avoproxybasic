$(document).ready(function() {
    addAnimate(".animated");
    $(window).scroll(function() {
        addAnimate(".animated");
    });
    $("body").on('click', '[href*="#"]', function(event) {
        event.preventDefault();
        let fixed_offset = 100;
        $('html,body').stop().animate({ scrollTop: $(this.hash).offset().top - fixed_offset }, 1000);
    });
    let buttonUp = $('.batton_top');
    buttonUp.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, 1000);
    });
    $(window).scroll(function() {
        if ($(window).scrollTop() > $(window).height()) {
            if (buttonUp.hasClass("visible")) return;
            buttonUp.addClass('visible');
        } else {
            buttonUp.removeClass('visible');
        }
    });

    function addAnimate(selector) {
        $(`${selector}`).each(function() {
            let elPos = $(this).offset().top;
            let topOfWindow = $(window).scrollTop();
            if (elPos < topOfWindow + $(window).height() - 75) {
                if ($(this).hasClass("fromLeft")) {
                    $(this).addClass("fadeInLeft removeOpacity");
                } else if ($(this).hasClass("fromTop")) {
                    $(this).addClass("fadeInDown removeOpacity");
                } else if ($(this).hasClass("fromRight")) {
                    $(this).addClass("fadeInRight removeOpacity");
                } else if ($(this).hasClass("fromBottom")) {
                    $(this).addClass("fadeInUp removeOpacity");
                }
            }
        })
    }

    function Utils() {

    }

    Utils.prototype = {
        constructor: Utils,
        isElementInView: function(element, fullyInView) {
            var pageTop = $(window).scrollTop();
            var pageBottom = pageTop + $(window).height();
            var elementTop = $(element).offset().top;
            var elementBottom = elementTop + $(element).height();

            if (fullyInView === true) {
                return ((pageTop < elementTop) && (pageBottom > elementBottom));
            } else {
                return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
            }
        }
    };
});